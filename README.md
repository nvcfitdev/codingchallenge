# iTunes Movie Finder
An Appetizer Apps coding challenge.
This app allows the User to search for a movie using the iTunes API.

## Tech Stack
- Kotlin
- MVVM as the Architecture
- LiveData, ViewModel and Coroutines
- Repository Pattern
- Room Database
- Retrofit
- DaggerHilt

## Why MVVM?
- MVVM is the best suggested architecture pattern to be used when using DaggerHilt. This way it becomes easier for lifecycle, business logic and UI persistence.

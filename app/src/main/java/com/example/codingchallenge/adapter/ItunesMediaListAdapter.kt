package com.example.codingchallenge.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.example.codingchallenge.R
import com.example.codingchallenge.domain.model.ItunesMedia
import javax.inject.Inject

class ItunesMediaListAdapter @Inject constructor(
    private val glide: RequestManager
) : RecyclerView.Adapter<ItunesMediaListAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivThumbnail: ImageView = itemView.findViewById(R.id.iv_thumbnail)
        val tvTrackName: TextView = itemView.findViewById(R.id.tv_trackName)
        val tvGenre: TextView = itemView.findViewById(R.id.tv_genre)
        val tvPrice: TextView = itemView.findViewById(R.id.tv_price)
        val ivFavorite: ImageView = itemView.findViewById(R.id.iv_favorite)
    }

    //List differ is a tool that takes 2 list and calculates the difference between those lists,
    //which enables our recyclerview to update items that are new in the new list.
    private val diffCallback = object : DiffUtil.ItemCallback<ItunesMedia>() {
        override fun areItemsTheSame(oldItem: ItunesMedia, newItem: ItunesMedia): Boolean {
            return oldItem.trackId == newItem.trackId
        }

        override fun areContentsTheSame(oldItem: ItunesMedia, newItem: ItunesMedia): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)
    var itunesMediaList: List<ItunesMedia>
        get() = differ.currentList
        set(value) = differ.submitList(value)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_itunes_media, parent, false)
        )
    }

    //Binding data from the object to the viewholders in the recyclerview
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itunesMedia = itunesMediaList[position]
        glide.load(itunesMedia.artworkUrl100).into(holder.ivThumbnail)
        holder.tvTrackName.text = itunesMedia.trackName
        holder.tvGenre.text = itunesMedia.primaryGenreName
        holder.tvPrice.text = "${itunesMedia.currency} ${itunesMedia.trackPrice}"

        //Change the heart drawable if itunesMedia is added in favorites
        if (itunesMedia.isFavorite) {
            holder.ivFavorite.setImageResource(R.drawable.ic_baseline_favorite_24)
        } else {
            holder.ivFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24)
        }

        //Separated on click listener for recyclerview item and addFavorite
        holder.ivFavorite.setOnClickListener {
            onFavoriteClickListener?.let { click ->
                click(itunesMedia)
            }
        }

        holder.itemView.setOnClickListener {
            onItemClickListener?.let { click ->
                click(itunesMedia)
            }
        }
    }

    private var onItemClickListener: ((ItunesMedia) -> Unit)? = null
    private var onFavoriteClickListener: ((ItunesMedia) -> Unit)? = null

    fun setItemClickListener(listener: (ItunesMedia) -> Unit) {
        onItemClickListener = listener
    }

    fun setFavoriteClickListener(listener: (ItunesMedia) -> Unit) {
        onFavoriteClickListener = listener
    }

    override fun getItemCount(): Int {
        return itunesMediaList.size
    }
}
package com.example.codingchallenge.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codingchallenge.db.Favorite
import com.example.codingchallenge.domain.model.ItunesMedia
import com.example.codingchallenge.repository.FavoriteRepository
import com.example.codingchallenge.repository.ItunesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ItunesViewModel @Inject constructor(
    private val itunesRepo: ItunesRepository,
    private val favoriteRepo: FavoriteRepository
): ViewModel() {
    //Create a mutable live data and immutable live data to restrict fragment or activities
    //from changing data from API
    private val _itunesMediaItems = MutableLiveData<List<ItunesMedia>>()
    val itunesMediaItems: LiveData<List<ItunesMedia>> = _itunesMediaItems

    //Call repository function from a coroutine scope since repo has suspend function
    init {
        search("Star", "United States", "Movie")
    }

    //Perform search request with the itunes-api and cross check values with favorites
    //saved in the room database, update value of domain model with the result of comparison
    fun search(term: String, country: String, media: String) {
        viewModelScope.launch(Dispatchers.IO) {
            itunesRepo.search(
                term,
                country,
                media).apply {
                if (this != null) {
                    for (itunesMedia in this) {
                        if (favoriteRepo.checkFavoriteInstances(itunesMedia.trackId!!) > 0) {
                            itunesMedia.isFavorite = true
                        }
                    }

                    _itunesMediaItems.postValue(this)
                }
            }
        }
    }

    //Set itunesMedia value to true or false and save or delete the favorite to room database
    fun updateItunesMedia(itunesMedia: ItunesMedia) {
        val position = _itunesMediaItems.value!!.indexOf(itunesMedia)

        if (_itunesMediaItems.value!![position].isFavorite) {
            _itunesMediaItems.value!![position].isFavorite = false
            deleteFavorite(Favorite(itunesMedia.trackId, itunesMedia.trackName))
        } else {
            _itunesMediaItems.value!![position].isFavorite = true
            addFavorite(Favorite(itunesMedia.trackId, itunesMedia.trackName))
        }
    }

    private fun addFavorite(favorite: Favorite) {
        viewModelScope.launch(Dispatchers.IO) {
            favoriteRepo.insertFavorite(favorite)
        }
    }

    private fun deleteFavorite(favorite: Favorite) {
        viewModelScope.launch(Dispatchers.IO) {
            favoriteRepo.deleteFavorite(favorite)
        }
    }
}
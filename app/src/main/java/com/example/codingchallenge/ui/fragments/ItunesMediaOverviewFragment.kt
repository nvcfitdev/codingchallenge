package com.example.codingchallenge.ui.fragments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.codingchallenge.R
import com.example.codingchallenge.databinding.FragmentMediaOverviewBinding
import com.example.codingchallenge.domain.model.ItunesMedia
import com.example.codingchallenge.ui.BaseBindFragment
import com.example.codingchallenge.ui.viewmodels.ItunesViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ItunesMediaOverviewFragment : BaseBindFragment<FragmentMediaOverviewBinding>() {

    private val viewModel: ItunesViewModel by activityViewModels()
    private var itunesMedia: ItunesMedia? = null

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToParent: Boolean
    ): FragmentMediaOverviewBinding = FragmentMediaOverviewBinding.inflate(layoutInflater)

    override fun initData() {
        //Retrieve parcelable bundle from arguments
        arguments?.getParcelable<ItunesMedia>("media")?.let {
            itunesMedia = it
        }
    }

    override fun initViews() {
        binding.tvBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

        if (itunesMedia != null) {
            //Bind data to the view components with kotlin synthetic
            binding.vvPreview.setVideoPath(itunesMedia!!.previewUrl)
            binding.vvPreview.setOnPreparedListener {
                binding.vvPreview.start()
                it.isLooping = true
            }
            binding.tvTrackName.text = itunesMedia!!.trackName
            binding.tvGenre.text = itunesMedia!!.primaryGenreName
            binding.tvLongDescription.text = itunesMedia!!.longDescription
            binding.tvPrice.text = "Price: ${itunesMedia!!.currency} ${itunesMedia!!.trackPrice}"

            //Set the color of heart depending on the value of isFavorite
            if (itunesMedia!!.isFavorite) {
                binding.ivFavorite.setImageResource(R.drawable.ic_baseline_favorite_24)
            } else {
                binding.ivFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24)
            }

            //Update value of mutable data in viewmodel and update UI of heart
            binding.ivFavorite.setOnClickListener {
                if (itunesMedia!!.isFavorite) {
                    binding.ivFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24)
                } else {
                    binding.ivFavorite.setImageResource(R.drawable.ic_baseline_favorite_24)
                }
                viewModel.updateItunesMedia(itunesMedia!!)
            }
        }
    }
}
package com.example.codingchallenge.ui.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codingchallenge.db.Visit
import com.example.codingchallenge.repository.VisitRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserVisitViewModel @Inject constructor(
    private val visitRepository: VisitRepository
): ViewModel() {

    fun insertVisit(visit: Visit) {
        viewModelScope.launch {
            visitRepository.insertVisit(visit)
        }
    }

    val visitsByDate = visitRepository.getAllVisits()
}
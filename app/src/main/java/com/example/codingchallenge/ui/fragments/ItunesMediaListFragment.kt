package com.example.codingchallenge.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.codingchallenge.R
import com.example.codingchallenge.adapter.ItunesMediaListAdapter
import com.example.codingchallenge.databinding.FragmentMediaListBinding
import com.example.codingchallenge.ui.BaseBindFragment
import com.example.codingchallenge.ui.viewmodels.ItunesViewModel
import com.example.codingchallenge.ui.viewmodels.UserVisitViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.Date
import javax.inject.Inject

@AndroidEntryPoint
class ItunesMediaListFragment : BaseBindFragment<FragmentMediaListBinding>() {

    private val viewModel: ItunesViewModel by viewModels()
    private val visitViewModel: UserVisitViewModel by viewModels()

    @Inject
    lateinit var itunesMediaListAdapter: ItunesMediaListAdapter

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        attachToParent: Boolean
    ): FragmentMediaListBinding = FragmentMediaListBinding.inflate(layoutInflater)

    override fun initViews() {
        subscribeToObservers()
        setUpRecyclerView()

        itunesMediaListAdapter.setItemClickListener {
            //Pass parcelable bundle to another fragment
            Bundle().apply {
                this.putParcelable("media", it)
            }.also {
                findNavController().navigate(R.id.viewItunesMedia, it)
            }
        }

        //On click listener for clicking the heart icon
        itunesMediaListAdapter.setFavoriteClickListener {
            //Update value of mutable data in viewmodel and notify data set changed
            //to update recyclerview
            viewModel.updateItunesMedia(it)
            binding.rvItunesMediaList.adapter?.notifyDataSetChanged()
        }

        //Added a search view for which modifies the term in the search query
        binding.svSearch.queryHint = "Search Here"
        binding.svSearch.setOnQueryTextListener(object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query.isNullOrBlank()) {
                    Toast.makeText(requireContext(), "Search can't be blank.", Toast.LENGTH_SHORT).show()
                } else {
                    viewModel.search(query, "us", "movie")
                }
                binding.svSearch.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })
    }

    override fun initData() {
        //Do nothing
    }

    private fun setUpRecyclerView() {
        binding.rvItunesMediaList.apply {
            adapter = itunesMediaListAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun subscribeToObservers() {
        //Observe data from api request
        viewModel.itunesMediaItems.observe(viewLifecycleOwner) { result ->
            if (result.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "Could not find anything.", Toast.LENGTH_SHORT).show()
            } else {
                itunesMediaListAdapter.itunesMediaList = result
            }
        }

        //Observe data stored in room database
        visitViewModel.visitsByDate.observe(viewLifecycleOwner) {
            if (it.size <= 1) {
                binding.tvLastVisit.text = "This is your first visit."
            } else {
                val sdf = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
                val netDate = Date(it[it.size-2].timeStamp)
                binding.tvLastVisit.text = "Last visit: ${sdf.format(netDate)}"
            }
        }

    }
}


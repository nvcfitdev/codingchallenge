package com.example.codingchallenge.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

//Abstracted base fragment class for reusability
abstract class BaseBindFragment<T : ViewBinding> : Fragment() {

    private var _binding: T? = null
    val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = getViewBinding(inflater, container, false).run {
        _binding = this
        binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
        initViews()
        initStatusBar()
    }

    open fun initStatusBar() {}

    abstract fun getViewBinding(inflater: LayoutInflater, container: ViewGroup?, attachToParent: Boolean) : T

    abstract fun initViews()

    abstract fun initData()

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}
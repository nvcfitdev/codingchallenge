package com.example.codingchallenge.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.codingchallenge.databinding.ActivityMainBinding
import com.example.codingchallenge.db.Visit
import com.example.codingchallenge.ui.viewmodels.UserVisitViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val userVisitViewModel: UserVisitViewModel by viewModels()

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        userVisitViewModel.insertVisit(Visit(timeStamp = System.currentTimeMillis()))
    }
}
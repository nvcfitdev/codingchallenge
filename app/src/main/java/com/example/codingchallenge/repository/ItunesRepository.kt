package com.example.codingchallenge.repository

import com.example.codingchallenge.domain.model.ItunesMedia

abstract class ItunesRepository : BaseHttpRepository() {
    abstract suspend fun search(
        term: String,
        country: String,
        media: String,
    ): List<ItunesMedia>?
}
package com.example.codingchallenge.repository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

//This is a base class to handle any API errors gracefully when calling thru the RepositoryImpl's
open class BaseHttpRepository {
    suspend fun <T> safeApiCall(
        apiCall: suspend () -> Response<T>
    ): HttpResult<T> {
        return withContext(Dispatchers.IO) {
            try {
                apiCall.invoke().let { response ->
                    when (response.code()) {
                        in 200..299 -> {
                            HttpResult.Success(
                                statusCode = response.code(),
                                data = response.body(),
                                message =  response.message()
                            )
                        }
                        else -> {
                            HttpResult.Failed(
                                statusCode = response.code(),
                                data = response.body(),
                                message = "failed"
                            )
                        }
                    }
                }
            } catch (e: Throwable) {
                HttpResult.Failed(-1, message = e.message)
            }
        }
    }
}

sealed class HttpResult<T>(
    val statusCode: Int? = -1,
    val data: T? = null,
    val message: String? = ""
) {

    class Success<T>(statusCode: Int, data: T?, message: String) :
        HttpResult<T>(statusCode, data, message)

    class Failed<T>(statusCode: Int, data: T? = null, message: String? = "failed") :
        HttpResult<T>(statusCode, data, message)
}
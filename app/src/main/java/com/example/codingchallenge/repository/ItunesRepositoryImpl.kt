package com.example.codingchallenge.repository

import com.example.codingchallenge.domain.model.ItunesMedia
import com.example.codingchallenge.network.ItunesMediaDTOMapper
import com.example.codingchallenge.service.ApiService

class ItunesRepositoryImpl(
    private val apiService: ApiService,
    private val mapper: ItunesMediaDTOMapper
): ItunesRepository() {

    override suspend fun search(term: String, country: String, media: String): List<ItunesMedia>? {
        return when (val result = safeApiCall { apiService.search(term, country, media) }) {
            is HttpResult.Success -> result.data?.results?.let { mapper.toDomainList(it) }
            else -> emptyList()
        }
    }
}
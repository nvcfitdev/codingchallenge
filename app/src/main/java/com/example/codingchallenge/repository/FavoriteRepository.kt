package com.example.codingchallenge.repository

import com.example.codingchallenge.db.Favorite
import com.example.codingchallenge.db.FavoriteDAO
import javax.inject.Inject

//Repository used for Favorited items
class FavoriteRepository @Inject constructor(
    private val favoriteDAO: FavoriteDAO
) {

    suspend fun insertFavorite(favorite: Favorite) {
        favoriteDAO.insertFavorite(favorite)
    }

    suspend fun deleteFavorite(favorite: Favorite) {
        favoriteDAO.deleteFavorite(favorite)
    }

    fun checkFavoriteInstances(id: Int): Int {
        return favoriteDAO.checkFavoriteInstances(id)
    }

//    fun getFavorites(): List<ItunesMedia> {
//        return favoriteDAO.getFavorites()
//    }
}
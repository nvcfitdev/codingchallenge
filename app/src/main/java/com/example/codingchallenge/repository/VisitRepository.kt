package com.example.codingchallenge.repository

import com.example.codingchallenge.db.Visit
import com.example.codingchallenge.db.VisitDAO
import javax.inject.Inject

class VisitRepository @Inject constructor(
    private val visitDao: VisitDAO
){
    suspend fun insertVisit(visit: Visit) = visitDao.insertUserVisit(visit)
    fun getAllVisits() = visitDao.getAllVisitsByDate()
}
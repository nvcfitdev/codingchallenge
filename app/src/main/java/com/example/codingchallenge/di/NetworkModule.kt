package com.example.codingchallenge.di

import com.example.codingchallenge.network.ItunesMediaDTOMapper
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

//This handles the network requests to the ITunes API Service.
@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit = Retrofit.Builder().apply {
        baseUrl("https://itunes.apple.com/")
            .addConverterFactory(
                GsonConverterFactory.create(GsonBuilder().apply {
                    setLenient()
                }.create())
            )
    }.build()

    @Singleton
    @Provides
    fun provideItunesMediaMapper(): ItunesMediaDTOMapper = ItunesMediaDTOMapper()
}
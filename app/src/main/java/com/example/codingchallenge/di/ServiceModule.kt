package com.example.codingchallenge.di

import com.example.codingchallenge.service.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ServiceModule {

    @Singleton
    @Provides
    fun provideRepositoryService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)
}
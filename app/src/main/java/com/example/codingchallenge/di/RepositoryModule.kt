package com.example.codingchallenge.di

import com.example.codingchallenge.network.ItunesMediaDTOMapper
import com.example.codingchallenge.repository.ItunesRepository
import com.example.codingchallenge.repository.ItunesRepositoryImpl
import com.example.codingchallenge.service.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

//This is a repository module that handles dependency injection to repositories
@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun provideItunesMediaRepository(
        apiService: ApiService,
        itunesMediaDtoMapper: ItunesMediaDTOMapper
    ): ItunesRepository = ItunesRepositoryImpl(apiService, itunesMediaDtoMapper)
}
package com.example.codingchallenge.di

import android.content.Context
import androidx.room.Room
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.codingchallenge.app.BaseApplication
import com.example.codingchallenge.db.FavoriteDB
import com.example.codingchallenge.db.VisitDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

private const val VISIT_DATABASE_NAME = "visit_db"
private const val FAVORITE_DATABASE_NAME = "favorite_db"

//A hilt module for providing dependency injection all through out the application
@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Singleton
    @Provides
    fun provideApplication(@ApplicationContext app: Context): BaseApplication {
        return app as BaseApplication
    }

    @Singleton
    @Provides
    fun provideGlideInstance(
        @ApplicationContext context: Context
    ) = Glide.with(context)
        .setDefaultRequestOptions(RequestOptions().diskCacheStrategy(DiskCacheStrategy.DATA))

    @Singleton
    @Provides
    fun provideVisitDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        VisitDB::class.java,
        VISIT_DATABASE_NAME
    ).build()

    @Singleton
    @Provides
    fun providesVisitDao(db: VisitDB) = db.getVisitDAO()

    @Singleton
    @Provides
    fun provideFavoriteDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        FavoriteDB::class.java,
        FAVORITE_DATABASE_NAME
    ).build()

    @Singleton
    @Provides
    fun providesFavoriteDao(db: FavoriteDB) = db.getFavoriteDao()
}
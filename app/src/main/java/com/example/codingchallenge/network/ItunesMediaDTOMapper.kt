package com.example.codingchallenge.network

import com.example.codingchallenge.domain.dto.ItunesMediaDTO
import com.example.codingchallenge.domain.model.ItunesMedia
import com.example.codingchallenge.util.DomainMapper

class ItunesMediaDTOMapper : DomainMapper<ItunesMediaDTO, ItunesMedia> {

    override fun mapToDomainModel(model: ItunesMediaDTO): ItunesMedia {
        return ItunesMedia(
            artistName = model.artistName,
            artworkUrl100 = model.artworkUrl100,
            artworkUrl30 = model.artworkUrl30,
            artworkUrl60 = model.artworkUrl60,
            collectionArtistId = model.collectionArtistId,
            collectionArtistViewUrl = model.collectionArtistViewUrl,
            collectionCensoredName = model.collectionCensoredName,
            collectionExplicitness = model.collectionExplicitness,
            collectionHdPrice = model.collectionHdPrice,
            collectionId = model.collectionId,
            collectionName = model.collectionName,
            collectionPrice = model.collectionPrice,
            collectionViewUrl = model.collectionViewUrl,
            contentAdvisoryRating = model.contentAdvisoryRating,
            country = model.country,
            currency = model.currency,
            discCount = model.discCount,
            discNumber = model.discNumber,
            hasITunesExtras = model.hasITunesExtras,
            kind = model.kind,
            longDescription = model.longDescription,
            previewUrl = model.previewUrl,
            primaryGenreName = model.primaryGenreName,
            releaseDate = model.releaseDate,
            shortDescription = model.shortDescription,
            trackCensoredName = model.trackCensoredName,
            trackCount = model.trackCount,
            trackExplicitness = model.trackExplicitness,
            trackHdPrice = model.trackHdPrice,
            trackHdRentalPrice = model.trackHdRentalPrice,
            trackId = model.trackId,
            trackName = model.trackName,
            trackNumber = model.trackNumber,
            trackPrice = model.trackPrice,
            trackRentalPrice = model.trackRentalPrice,
            trackTimeMillis = model.trackTimeMillis,
            trackViewUrl = model.trackViewUrl,
            wrapperType = model.wrapperType
        )
    }

    override fun mapFromDomainModel(domainModel: ItunesMedia): ItunesMediaDTO {
        return ItunesMediaDTO(
            artistName = domainModel.artistName.toString(),
            artworkUrl100 = domainModel.artworkUrl100.toString(),
            artworkUrl30 = domainModel.artworkUrl30.toString(),
            artworkUrl60 = domainModel.artworkUrl60.toString(),
            collectionArtistId = domainModel.collectionArtistId ?: 0,
            collectionArtistViewUrl = domainModel.collectionArtistViewUrl.toString(),
            collectionCensoredName = domainModel.collectionCensoredName.toString(),
            collectionExplicitness = domainModel.collectionExplicitness.toString(),
            collectionHdPrice = domainModel.collectionHdPrice ?: 0.0,
            collectionId = domainModel.collectionId ?: 0,
            collectionName = domainModel.collectionName.toString(),
            collectionPrice = domainModel.collectionPrice ?: 0.0 ,
            collectionViewUrl = domainModel.collectionViewUrl.toString(),
            contentAdvisoryRating = domainModel.contentAdvisoryRating.toString(),
            country = domainModel.country.toString(),
            currency = domainModel.currency.toString(),
            discCount = domainModel.discCount ?: 0,
            discNumber = domainModel.discNumber ?: 0,
            hasITunesExtras = domainModel.hasITunesExtras ?: false,
            kind = domainModel.kind.toString(),
            longDescription = domainModel.longDescription.toString(),
            previewUrl = domainModel.previewUrl.toString(),
            primaryGenreName = domainModel.primaryGenreName.toString(),
            releaseDate = domainModel.releaseDate.toString(),
            shortDescription = domainModel.shortDescription.toString(),
            trackCensoredName = domainModel.trackCensoredName.toString(),
            trackCount = domainModel.trackCount ?: 0,
            trackExplicitness = domainModel.trackExplicitness.toString(),
            trackHdPrice = domainModel.trackHdPrice ?: 0.0,
            trackHdRentalPrice = domainModel.trackHdRentalPrice ?: 0.0,
            trackId = domainModel.trackId ?: 0,
            trackName = domainModel.trackName.toString(),
            trackNumber = domainModel.trackNumber ?: 0,
            trackPrice = domainModel.trackPrice ?: 0.0,
            trackRentalPrice = domainModel.trackRentalPrice ?: 0.0,
            trackTimeMillis = domainModel.trackTimeMillis ?: 0,
            trackViewUrl = domainModel.trackViewUrl.toString(),
            wrapperType = domainModel.wrapperType.toString()
        )
    }

    fun toDomainList(initial: List<ItunesMediaDTO>): List<ItunesMedia> {
        return initial.map { mapToDomainModel(it) }
    }

    fun fromDomainList(initial: List<ItunesMedia>): List<ItunesMediaDTO> {
        return initial.map { mapFromDomainModel(it) }
    }

}

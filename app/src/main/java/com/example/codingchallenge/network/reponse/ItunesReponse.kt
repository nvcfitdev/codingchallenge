package com.example.codingchallenge.network.reponse

import androidx.annotation.Keep
import com.example.codingchallenge.domain.dto.ItunesMediaDTO

@Keep
data class ItunesMediaResponse (
    val resultCount: Int,
    val results: List<ItunesMediaDTO>
)
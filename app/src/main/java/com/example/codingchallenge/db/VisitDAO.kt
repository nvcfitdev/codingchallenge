package com.example.codingchallenge.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface VisitDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUserVisit(visit: Visit)

    @Query("SELECT * FROM visit_table ORDER BY timeStamp ASC")
    fun getAllVisitsByDate(): LiveData<List<Visit>>
}
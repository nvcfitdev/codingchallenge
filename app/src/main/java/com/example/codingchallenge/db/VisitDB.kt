package com.example.codingchallenge.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [Visit::class],
    version = 1,
    exportSchema = false
)
abstract class VisitDB : RoomDatabase() {
    abstract fun getVisitDAO(): VisitDAO
}
package com.example.codingchallenge.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "visit_table")
data class Visit(
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null,
    var timeStamp: Long = 0L
)
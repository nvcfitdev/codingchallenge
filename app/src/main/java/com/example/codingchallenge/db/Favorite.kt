package com.example.codingchallenge.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

//Room database entity for favorite only saves id and name of itunes media
@Entity(tableName = "favorite_table")
class Favorite(
    @PrimaryKey
    @ColumnInfo(name = "trackId") val trackId: Int?,
    @ColumnInfo(name = "trackName") val trackName: String?,
)
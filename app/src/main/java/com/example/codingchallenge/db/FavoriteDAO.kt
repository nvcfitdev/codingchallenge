package com.example.codingchallenge.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

//Database that handles all Queries of User Favorites
@Dao
interface FavoriteDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFavorite(favorite: Favorite)

    @Delete
    suspend fun deleteFavorite(favorite: Favorite)

    @Query("SELECT * FROM favorite_table where trackId = :id")
    fun checkFavoriteInstances(id: Int): Int

//    @Query("SELECT * FROM favorite_table")
//    fun getFavorites(): List<ItunesMedia>
}
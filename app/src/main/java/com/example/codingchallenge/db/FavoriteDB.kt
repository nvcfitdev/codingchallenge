package com.example.codingchallenge.db

import androidx.room.Database
import androidx.room.RoomDatabase

//An abstract class for creating an instance of the database with room
@Database(
    entities = [Favorite::class],
    version = 1,
    exportSchema = false
)
abstract class FavoriteDB : RoomDatabase() {

    abstract fun getFavoriteDao(): FavoriteDAO

}
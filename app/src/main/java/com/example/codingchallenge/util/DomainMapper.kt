package com.example.codingchallenge.util

interface DomainMapper<T, DomainModel> {
    fun mapToDomainModel(model: T): DomainModel
    fun mapFromDomainModel(dModel: DomainModel): T
}
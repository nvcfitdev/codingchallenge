package com.example.codingchallenge.service

import com.example.codingchallenge.network.reponse.ItunesMediaResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("search")
    suspend fun search(
        @Query("term") term: String,
        @Query("country") country: String,
        @Query("media") media: String
    ): Response<ItunesMediaResponse>
}